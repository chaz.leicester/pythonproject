from flask import Flask, request, json
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    data = json.loads(request.data)

    predictedSales = predictSales(data)

    #finally return the predicted sales
    return {"sales": predictedSales}

#LOGIC to predict sales
def predictSales(json):
    # STORE values based from the payload
    store = json["Store"]
    dayOfWeek = json["DayOfWeek"]
    date = json["Date"]
    sales = json["Sales"]
    customers = json["Customers"]
    open = json["Open"]
    promo = json["Promo"]
    stateHoliday = json["StateHoliday"]
    schoolHoliday = json["SchoolHoliday"]


    predictedSales = 12345.12 #CHANGE THIS TO ACTUAL LOGIC
    return  predictedSales

if __name__ == "__main__":
    app.run(host='0.0.0.0')
